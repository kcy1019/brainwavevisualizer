#-*-coding:utf-8-*-
import matplotlib as mpl
mpl.use('Agg')

from matplotlib import pyplot as plt
from matplotlib.patches import Rectangle
import numpy as np
import struct
import os
from io import BytesIO
import base64


def figure_to_base64(fig):
    """
    Encode given figure into base64(image/png;base64,{data}) for inline display.
    """
    figfile = BytesIO()
    fig.savefig(figfile, format='png')
    figfile.seek(0)
    return base64.b64encode(figfile.getvalue())


def get_filesize(f):
    """
    Get size of given filelike object in bytes.
    """
    old_file_position = f.tell()
    f.seek(0, os.SEEK_END)
    size = f.tell()
    f.seek(old_file_position, os.SEEK_SET)
    return size


def parse_time(xx):
    return ["{:02d}:{:04.1f}".format(int(x // 60), x % 60) for x in xx]


def get_specgram(filename, t_from, t_to, t_curr,
        width = 800, height = 300, dpi = 96, ticks = 10, alpha = 0.20):
    """
    Plot spectrogram of signals from `t_from` to `t_to`(both in seconds).
    Sampling Freq.: 500Hz, File Format: (8B timestamp) + n * (2B signal).

    Args:
        t_from (float): start time in seconds.
        t_to   (float): end time in seconds.

    Returns:
        Base64-encoded specgram in image/png.
    """

    with open(filename, 'rb') as fin:
        ts = struct.unpack('l', fin.read(8))
        n = (get_filesize(fin) - 8) / 2
        wave = np.array(struct.unpack("<%dh" % n, fin.read()), dtype=np.short)

    if t_from < 0.0: t_from = 0
    if t_to >= len(wave) / 500.: t_to = len(wave) / 500.

    fig = plt.figure(figsize = (width / dpi, height / dpi), dpi = dpi)
    _fig = plt.subplot(111)

    _fig.specgram(wave, NFFT=250, Fs = 500, noverlap= 125)
    _fig.set_ylim(ymin = 20., ymax = 80.)
    _fig.set_xlim(xmin = t_from, xmax = t_to)

    y = np.linspace(20., 80., 7)
    yticks = [str(a) for a in np.linspace(20., 80., 7)]
    _fig.set_yticks(y, minor = False)
    _fig.set_yticklabels(yticks, fontdict = None, minor = False)

    x = np.linspace(t_from, t_to, ticks)
    xticks = parse_time(x)
    _fig.set_xticks(x, minor=False)
    _fig.set_xticklabels(xticks, fontdict=None, minor=False)

    _fig.hold(True)
    _fig.add_patch(Rectangle((t_curr, 20.), 0.5, 80, facecolor="black", alpha = alpha))

    ret = figure_to_base64(fig)

    fig.clf()
    plt.close(fig)
    plt.close('all')

    return ret
