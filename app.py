#-*-coding:utf-8-*-
from flask import Flask, request, render_template
from specgram import get_specgram
from os import path, listdir
from flask_socketio import SocketIO, send, emit
import data_cut as dc
import json


BASE_PATH = path.join('static', '1')
STATIC_PATH = '/brainwave/static/1'
app = Flask(__name__)
app.debug = True # Should be disabled later!
app.config['SCERET_KEY'] = 'why-so-secret?'
socketio = SocketIO(app, message_queue='redis://')


@app.route('/brainwave/')
def browse():
    return render_template('list.html',
            ids = [f[:-4] for f in listdir(BASE_PATH) if f.endswith('mp4')],
            base_url = '/brainwave')


@app.route('/brainwave/<video_id>')
def view(video_id):
    spath = BASE_PATH + '/{}.dat'.format(video_id)
    return render_template('player.html',
            vid = video_id,
            vpath = STATIC_PATH + '/{}.mp4'.format(video_id),
            base_url = '/brainwave',
            tstamp = str(dc.read_signal(spath)[3]))


@app.route('/brainwave/request-sync/<video_id>/<int:sync_start>')
def resync_video(video_id, sync_start):
    res = dc.cut_data(BASE_PATH, video_id, dc.convert_timeformat(sync_start))
    return json.dumps(res)


@app.route('/brainwave/request-sync/<video_id>/<int:sync_start>/forcefully')
def resync_video_forced(video_id, sync_start):
    res = dc.cut_data(BASE_PATH, video_id, dc.convert_timeformat(sync_start), forced = True)
    return json.dumps(res)


def specgram(video_id, t_from, t_to, t_curr):
    t_from = float(t_from)
    t_to = float(t_to)
    t_curr = float(t_curr)

    return get_specgram(path.join(BASE_PATH, '{}.dat'.format(video_id)),
            t_from, t_to, t_curr)


@socketio.on('specgram')
def specgram_socket(video_id, t_from, t_to, t_curr):
    emit('receive-spec', r'{}|--|'.format(t_curr) + specgram(video_id, t_from, t_to, t_curr))


@app.route('/brainwave/load-tag/<video_id>')
def load_tag(video_id):
    pass


@app.route('/brainwave/save-tag/<video_id>', methods=['POST'])
def save_tag(video_id):
    pass


def main():
    socketio.run(app, host = '0.0.0.0', port = 4444)


if __name__ == "__main__":
    main()
