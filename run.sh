#!/bin/bash

gunicorn --worker-class eventlet -w 3 --bind 0.0.0.0:4444 --timeout 3600 wsgi
