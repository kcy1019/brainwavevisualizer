#-*-coding:utf-8-*-
from datetime import datetime
from moviepy import editor as mpy
import os
import struct
import shutil


def convert_timeformat(tstamp):
    tobj = datetime.fromtimestamp(tstamp / 1000.0)
    ts = 0.0

    #                min  sec   microsec
    ts += tobj.hour * 60 * 60
    ts += tobj.minute    * 60
    ts += tobj.second
    ts += tobj.microsecond    / 1000000.0

    return ts


def get_filesize(f):
    """
    Get size of given filelike object in bytes.
    """
    old_file_position = f.tell()
    f.seek(0, os.SEEK_END)
    size = f.tell()
    f.seek(old_file_position, os.SEEK_SET)
    return size


def read_signal(filename):
    with open(filename, 'rb') as fin:
        tstamp = struct.unpack('<q', fin.read(8))[0]
        n = (get_filesize(fin) - 8) / 2
        wave = list(struct.unpack("<%dh" % n, fin.read()))

    tobj = datetime.fromtimestamp(tstamp / 1000.0)

    return (convert_timeformat(tstamp), n, wave, tstamp)


def read_video(filename):
    vid = mpy.VideoFileClip(filename)
    return (vid.end, vid)


def cut_signals(filename, t_from, suffix = '_SYNCED'):
    """
    Cut signal file according to given start time.
    Save with additional suffix at the end of the name.
    """
    ts, n, wave, tstamp = read_signal(filename)
    new_name = filename[:-4] + suffix + '.dat'
    skipped = int((t_from - ts) // 0.002)

    print ts, t_from, skipped, tstamp, tstamp + skipped * 2

    if skipped < 0:
        return (False, 'That\'s too early({}={}-{})'.format(skipped, ts, t_from))

    # try:
    with open(new_name, 'wb') as fout:
        fout.write(struct.pack('<q', tstamp + skipped * 2))
        fout.write(struct.pack('<%dh' % (n - skipped), *wave[skipped:]))
    return (True, new_name)
    # except:
    return (False, 'No such file')


def cut_video(filename, t_from, suffix = '_SYNCED'):
    """
    Cut video file according to given start time.
    Save with additional suffix at the end of the name.

    Returns: tuple (success ? True : False, success ? new_name : error_message)
    """

    try:
        vid = read_video(filename)[1]
        new_name = filename[:-4] + suffix + '.mp4'
        vid.subclip(t_from, vid.end).write_videofile(new_name, audio = False)
        return (True, new_name)
    except:
        return (False, 'No such file')


def cut_data(base_path, video_id, t_from, suffix = '_SYNCED', forced = False):
    vid_new_name = base_path + '/' + video_id + suffix + '.mp4'
    sig_new_name = base_path + '/' + video_id + suffix + '.dat'

    if not forced and os.path.isfile(vid_new_name) and os.path.isfile(sig_new_name):
        return {'success': False,
                'error': 'exists'}

    vid_name = base_path + '/' + video_id + '.mp4'
    sig_name = base_path + '/' + video_id + '.dat'

    vid_start = t_from

    ts, n = read_signal(sig_name)[:2]
    vlen = read_video(vid_name)

    if t_from < ts:
        t_from = ts

    delta = t_from - ts
    if delta % 0.002: # Sampling Rate
        t_from += 0.002 - (delta % 0.002)

    sig_res = cut_signals(sig_name, t_from)

    if vid_start - t_from != 0: # do it only when necessary.
        vid_res = cut_video(vid_name, abs(vid_start - t_from))
    else:
        shutil.copyfile(vid_name, vid_new_name)

    if sig_res[0] and vid_res[0]:
        return {'success': True,
                'signal': sig_res,
                'video': vid_res}
    elif sig_res[0]:
        return {'success': False,
                'error': 'video::' + vid_res[1]}
    elif vid_res[0]:
        return {'success': False,
                'error': 'signal::' + sig_res[1]}
    else:
        return {'success': False,
                'error': 'total failure'}
