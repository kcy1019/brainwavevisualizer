$(function() {
    UPDATE_INTERVAL = 0.5;

    console.log('Loading Page..');

    if (location.pathname.indexOf('_SYNCED') >= 0) {
        // Someone already synced this item.
    } else {
        // Request new version of this dataset.
        var forcefully = '';
        $('#applySync').click(function() {
            var self = this;
            var t = $('#syncText').val();
            if (t.length) {
                $(self).text('Running')
                $(self).prop('disabled', true);
                $.get(BASE_URL + '/request-sync/' + VIDEO_ID +
                      '/' + t + forcefully, function(data) {
                    if (data.success == true) {
                        $(self).text('Apply');
                        $(self).prop('disabled', false);
                        location.href = 'https://' + document.domain + BASE_URL;
                    } else if (data.error && data.error.indexOf('exists') != -1) {
                        alert('There IS synced version of self video.'+'\n'+
                              'Do you really want to re-sync self?'+'\n'+
                              'If then, click the button again.');
                        $(self).text('Apply(forcefully)');
                        forcefully = '/forcefully';
                        $(self).prop('disabled', false);
                    } else {
                        alert(data.error);
                    }
                }, 'json');
            }
        });
    }

    var socket = io.connect('https://' + document.domain);
    socket.on('connect', function(socket) {
        console.log('WebSocket: Just connected!');
        console.log(socket);
    });

    var Context = this;

    var updateSpectrogram = function(current_time) {
        if (Context.last_update === undefined) { Context.last_update = -1; }
        if (Math.abs(current_time - Context.last_update) < UPDATE_INTERVAL) { return; }

        socket.emit('specgram',
            VIDEO_ID,
            current_time - 10,
            current_time + 10,
            current_time
        );

        Context.last_update = current_time;
    };

    socket.on('receive-spec', function(_data) {
        var data = _data.split('|--|');
        Context.last_update = +data[0];
        if (Math.abs(Context.current_time - data[0]) > UPDATE_INTERVAL) {
            console.log(Context.current_time + "/" + data[0]);
            return;
        }
        $('#specgram').attr('src', 'data:image/png;base64,' + data[1]);
    });

    var player = videojs('video-main', { /* Options */ }, function() {
        console.log('Good to go!');

        var self = this;

        self.on('playing', function(e) {
            Context.last_update = undefined;
            Context.current_time = self.currentTime();
            updateSpectrogram(Context.current_time);
        });

        self.on('timeupdate', function(e) {
            Context.current_time = self.currentTime();
            updateSpectrogram(Context.current_time);
        });
    });

    console.log('Loading Complete!');

});
